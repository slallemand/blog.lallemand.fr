FROM python:3-slim AS builder
LABEL maintainer dev@lallemand.fr

WORKDIR /usr/src/app

COPY . .

RUN pip install --no-cache-dir pelican markdown && \
    ls -l themes && \
    pelican content -o /usr/src/app/output -s /usr/src/app/pelicanconf.py -t /usr/src/app/themes/plumage/


FROM nginx AS engine
LABEL maintainer dev@lallemand.fr

RUN apt-get update && apt-get -y dist-upgrade

COPY --from=builder /usr/src/app/output /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
