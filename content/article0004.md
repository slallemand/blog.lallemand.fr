Title: Redémarrage automatique d'un service par systemd
Tags: systemd, restart
lang: fr
Date: 2017-02-21 11:10:22

Par moment, un service démarré par systemd ne démarre pas correctement un autre service n'a pas encore démarré.  On peut mettre une dépendance entre services mais ça peut ne pas suffire.<br>
J'ai par exemple eu le cas avec haproxy qui redirige ses requêtes sur des containeurs LXC.<br>
Dans ce cas, on va juste demander à systemd de faire plusieurs essais de démarrage de haproxy.<br>

On édite le fichier /etc/systemd/system/multi-user.target.wants/haproxy.service et on ajoute dans les lignes suivantes dans la partie Service :

    :::bash
    Restart=always
    RestartSec=30
    StartLimitInterval=200
    StartLimitBurst=5

Là, on va faire 5 essais espacés chacun de 30 secondes.

On recharge la configuration de systemd :

    :::bash
    systemctl daemon-reload


Le fichier complet pour information :

    :::bash
    [Unit]
    Description=HAProxy Load Balancer
    Documentation=man:haproxy(1)
    Documentation=file:/usr/share/doc/haproxy/configuration.txt.gz
    After=network.target syslog.service
    Wants=syslog.service
    
    [Service]
    Environment=CONFIG=/etc/haproxy/haproxy.cfg
    EnvironmentFile=-/etc/default/haproxy
    ExecStartPre=/usr/sbin/haproxy -f ${CONFIG} -c -q
    ExecStart=/usr/sbin/haproxy-systemd-wrapper -f ${CONFIG} -p /run/haproxy.pid $EXTRAOPTS
    ExecReload=/usr/sbin/haproxy -c -f ${CONFIG}
    ExecReload=/bin/kill -USR2 $MAINPID
    KillMode=mixed
    Restart=always
    RestartSec=30
    StartLimitInterval=200
    StartLimitBurst=5
    
    [Install]
    WantedBy=multi-user.target
    

