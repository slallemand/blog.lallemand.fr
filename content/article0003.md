Title: Utilisation de UFW pour redirection de ports
Tags: ufw , firewall ,forward
lang: fr
Date: 2017-02-04 22:46

Redirection d'un port avec UFW.


    :::bash
    ufw allow 4242

Editer le fichier /etc/ufw/before.rules et ajouter au début du fichier :

    :::bash
    *nat
    :PREROUTING ACCEPT [0:0]
    -A PREROUTING -i eth0 -d a.b.c.d -p tcp --dport 4242 -j DNAT --to-destination e.f.g.h:22
    COMMIT
    

